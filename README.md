# Forkio


## Учасники проекту

### 1. Смирнов Єгор

* Шапка сайту з верхнім меню;
* Секція `People Are Talking About Fork`;
    
### 2. Щігров Лев

* Блок `Revolutionary Editor`;
* Секція `Here is what you get`;
* Секція `Fork Subscription Pricing`.

## Використані технології

* HTML/CSS;
* JS;
* SCSS;
* Git
* Gulp;